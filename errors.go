package cfsslapi

import (
	"fmt"
	"strconv"

	errors "golang.org/x/xerrors"
)

func errorString(code string) (string, error) {
	n, err := strconv.Atoi(code)
	if err != nil {
		return "", errors.Errorf("errorString: %w", err)
	}

	switch n {
	case 1000:
		return "CertificateError - Unknown", nil
	case 1001:
		return "CertificateError - ReadFailed", nil
	case 1002:
		return "CertificateError - DecodeFailed", nil
	case 1003:
		return "CertificateError - ParseFailed", nil
	case 1100:
		return "CertificateError - SelfSigned", nil
	case 1210:
		return "CertificateError - VerifyFailed - CertificateInvalid - NotAuthorizedToSign", nil
	case 1211:
		return "CertificateError - VerifyFailed - CertificateInvalid - Expired", nil
	case 1212:
		return "CertificateError - VerifyFailed - CertificateInvalid - CANotAuthorizedForThisName", nil
	case 1213:
		return "CertificateError - VerifyFailed - CertificateInvalid - TooManyIntermediates", nil
	case 1214:
		return "CertificateError - VerifyFailed - CertificateInvalid - IncompatibleUsage", nil
	case 1220:
		return "CertificateError - VerifyFailed - UnknownAuthority", nil
	}

	return "", fmt.Errorf("Uknown error code")
}
