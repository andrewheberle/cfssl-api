package cfsslapi

import (
	"encoding/json"

	errors "golang.org/x/xerrors"
)

/*
THE CERTINFO ENDPOINT

Endpoint: /api/v1/cfssl/certinfo
Method:   POST

Required parameters:

        One of the following parameters is required.

        * certificate: the PEM-encoded certificate to be parsed.
        * domain: a domain name indicating a remote host to retrieve a
          certificate for.
        * serial and authority_key_id: a certificate serial number and a
          matching authority key to look for in the database

Result:

	The certinfo endpoint returns a JSON object with the following
	keys:

        * subject contains a JSON object corresponding to a PKIX Name, including:
            * common_name
            * serial_number
            * country
            * organization
            * organizational_unit
            * locality
            * province
            * street_address
            * postal_code
            * names
            * extra_names
        * sans is a list of Subject Alternative Names.
        * not_before is the certificate's start date.
        * not_after is the certificate's end date.
        * sigalg is the signature algorithm used to sign the certificate.
*/

type certInfoRequest struct {
	Certificate  string `json:"certificate,omitempty"`
	Domain       string `json:"domain,omitempty"`
	Serial       string `json:"serial,omitempty"`
	AuthorityKey string `json:"authority_key_id,omitempty"`
}

type CertInfoResponse struct {
	Subject struct {
		CommonName string   `json:"common_name"`
		Serial     string   `json:"serial_number"`
		Country    string   `json:"country"`
		Org        string   `json:"organization"`
		OU         string   `json:"organizational_unit"`
		Locality   string   `json:"locality"`
		Province   string   `json:"province"`
		Address    string   `json:"street_address"`
		Postcode   string   `json:"postal_code"`
		Names      []string `json:"names"`
		ExtraNames []string `json:"extra_names"`
	} `json:"subject"`
	Sans               []string `json:"sans"`
	NotBefore          string   `json:"not_before"`
	NotAfter           string   `json:"not_after"`
	SignatureAlgorithm string   `json:"sigalg"`
}

func (api *ApiClient) CertInfoDomain(domain string) (resp CertInfoResponse, err error) {
	return api.certInfo(certInfoRequest{
		Domain: domain,
	})
}

func (api *ApiClient) CertInfoCertificate(certificate string) (resp CertInfoResponse, err error) {
	return api.certInfo(certInfoRequest{
		Certificate: certificate,
	})
}

func (api *ApiClient) CertInfoSerialAki(serial, aki string) (resp CertInfoResponse, err error) {
	return api.certInfo(certInfoRequest{
		Serial:       serial,
		AuthorityKey: aki,
	})
}

func (api *ApiClient) certInfo(req certInfoRequest) (resp CertInfoResponse, err error) {
	var endpoint = "certinfo"

	r, err := api.doRequest(req, endpoint)
	if err != nil {
		return resp, errors.Errorf("%s: %w", endpoint, err)
	}

	err = json.Unmarshal(r.Result, &resp)
	if err != nil {
		return resp, errors.Errorf("%s: %w", endpoint, err)
	}

	return resp, nil
}
