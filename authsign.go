package cfsslapi

/*
THE AUTHENTICATED SIGNING ENDPOINT

Endpoint: /api/v1/cfssl/authsign
Method:   POST

Required parameters:

    * token: the authentication token
    * request: an encoded JSON signing request (e.g. as
           documented in endpoint_sign.txt).

Optional parameters:

    The following parameters might be used by the authenticator
    as part of the authentication process.

    * timestamp: a Unix timestamp
    * remote_address: an address used in making the request.
    * bundle: a boolean specifying whether to include an "optimal"
    certificate bundle along with the certificate

Result:

    The returned result is a JSON object with a single key:

    * certificate: a PEM-encoded certificate that has been signed
    by the server.
    * bundle: See the result of endpoint_bundle.txt (only included if the bundle parameter was set)

The authentication documentation contains more information about how
authentication with CFSSL works.
*/

import (
	"encoding/json"

	"github.com/cloudflare/cfssl/auth"

	errors "golang.org/x/xerrors"
)

type authSignRequest struct {
	Token   []byte `json:"token"`
	Request []byte `json:"request"`
}

func (api *ApiClient) authSign(req authSignRequest) (resp SignResponse, err error) {
	var endpoint = "authsign"

	r, err := api.doRequest(req, endpoint)
	if err != nil {
		return resp, errors.Errorf("%s: %w", endpoint, err)
	}

	err = json.Unmarshal(r.Result, &resp)
	if err != nil {
		return resp, errors.Errorf("%s: %w", endpoint, err)
	}

	return resp, nil
}

func newAuthRequest(key string, req signRequest) (authreq authSignRequest, err error) {
	provider, err := auth.New(key, nil)
	if err != nil {
		return authSignRequest{}, errors.Errorf("newAuthRequest: auth.New: %w", err)
	}
	authreq.Request, err = json.Marshal(req)
	if err != nil {
		return authSignRequest{}, errors.Errorf("newAuthRequest: json.Marshal: %w", err)
	}

	authreq.Token, err = provider.Token(authreq.Request)
	if err != nil {
		return authSignRequest{}, errors.Errorf("newAuthRequest: provider.Token: %w", err)
	}

	return
}

func (api *ApiClient) AuthSign(key, csr string, options interface{}) (resp SignResponse, err error) {
	req, err := newAuthRequest(key, newSignRequest(csr, options))
	if err != nil {
		return SignResponse{}, errors.Errorf("AuthSign: %w", err)
	}

	return api.authSign(req)
}
