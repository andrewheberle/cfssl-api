module gitlab.com/andrewheberle/cfssl-api

go 1.13

require (
	github.com/cloudflare/cfssl v0.0.0-20190911221928-1a911ca1b1d6
	github.com/mitchellh/go-homedir v1.1.0
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.3.2
	golang.org/x/xerrors v0.0.0-20191011141410-1b5146add898
)
