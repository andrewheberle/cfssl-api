package cfsslapi

/*
INTRODUCTION TO THE CFSSL API

The CFSSL API allows applications to access the functionality of CFSSL
over an unauthenticated HTTP connection. By default, the API is
unauthenticated, it is important to understand that the CFSSL API
server must be running in a trusted environment in this case.

There are currently nine endpoints, each of which may be found under
the path `/api/v1/cfssl/<endpoint>`. The documentation for each
endpoint is found in the `doc/api` directory in the project source
under the name `endpoint_<endpoint>`. These nine endpoints are:

      - authsign: authenticated signing endpoint
      - bundle: build certificate bundles
      - crl: generates a CRL out of the certificate DB
      - info: obtain information about the CA, including the CA
        certificate
      - init_ca: initialise a new certificate authority
      - newkey: generate a new private key and certificate signing
        request
      - newcert: generate a new private key and certificate
      - scan: scan servers to determine the quality of their TLS set up
      - scaninfo: list options for scanning
      - sign: sign a certificate

RESPONSES

Responses take the form of the new CloudFlare API response format:

       {
         "result": <some data>,
         "success": true,
         "errors": [],
         "messages": [],
       }

Both the "messages" and "errors" fields have the same general format:
a message or error has the form

       {
         "code:" 1234,
         "message": "Informative message."
       }

If "success" is not "true", the result should be discarded, and the
errors examined to determine what happened. The CFSSL error codes are
documented in the `doc/errors.txt` file in the project source.
*/

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	errors "golang.org/x/xerrors"
)

const (
	ApiPath = "/api/v1/cfssl"
)

func NewApiClient(addr string, tls *tls.Config) *ApiClient {
	return &ApiClient{
		Address:   addr,
		TLSConfig: tls,
	}
}

type ApiClient struct {
	Address   string
	TLSConfig *tls.Config
}

type ResponseMessage struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

type rawResponse struct {
	Result   json.RawMessage   `json:"result"`
	Success  bool              `json:"success"`
	Errors   []ResponseMessage `json:"errors"`
	Messages []ResponseMessage `json:"messages"`
}

type Response struct {
	Result   interface{}       `json:"result"`
	Success  bool              `json:"success"`
	Errors   []ResponseMessage `json:"errors"`
	Messages []ResponseMessage `json:"messages"`
}

func (api *ApiClient) doRequest(req interface{}, endpoint string) (resp rawResponse, err error) {
	var b []byte

	switch req.(type) {
	case infoRequest:
		if endpoint == "info" {
			b, err = json.Marshal(req.(infoRequest))
		} else {
			err = fmt.Errorf("invalid request type for endpoint")
		}
	case certInfoRequest:
		if endpoint == "certinfo" {
			b, err = json.Marshal(req.(certInfoRequest))
		} else {
			err = fmt.Errorf("invalid request type for endpoint")
		}
	case revokeRequest:
		if endpoint == "revoke" {
			b, err = json.Marshal(req.(revokeRequest))
		} else {
			err = fmt.Errorf("invalid request type for endpoint")
		}
	case signRequest:
		if endpoint == "sign" {
			b, err = json.Marshal(req.(signRequest))
		} else {
			err = fmt.Errorf("invalid request type for endpoint")
		}
	case authSignRequest:
		if endpoint == "authsign" {
			b, err = json.Marshal(req.(authSignRequest))
		} else {
			err = fmt.Errorf("invalid request type for endpoint")
		}
	default:
		err = fmt.Errorf("unsupported request type")
	}

	if err != nil {
		return resp, errors.Errorf("doRequest: json.Marshal: %w", err)
	}

	response, err := api.post(endpoint, b)
	if err != nil {
		return resp, errors.Errorf("doRequest: %w", err)
	}

	err = json.Unmarshal(response, &resp)
	if err != nil {
		return resp, errors.Errorf("doRequest: json.Unmarshal: %w", err)
	}

	if !resp.Success {
		return resp, fmt.Errorf("doRequest: an error occurred")
	}

	return resp, nil
}

func (api *ApiClient) getScheme() string {
	if api.TLSConfig != nil {
		return "https"
	}

	return "http"
}

func (api *ApiClient) getUrl(endpoint string) string {
	return fmt.Sprintf("%s://%s%s/%s", api.getScheme(), api.Address, ApiPath, endpoint)
}

func (api *ApiClient) post(endpoint string, postreq []byte) (postres []byte, err error) {
	client := &http.Client{}

	buf := bytes.NewReader(postreq)

	url := api.getUrl(endpoint)

	resp, err := client.Post(url, "application/json", buf)
	if err != nil {
		return nil, errors.Errorf("post: %w", err)
	}
	defer resp.Body.Close()

	// Check return status code
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("post: invalid status of %d for %s", resp.StatusCode, url)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, errors.Errorf("post: %w", err)
	}

	return body, nil
}
