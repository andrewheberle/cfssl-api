package cmd

import (
	"fmt"
	"strings"

	"github.com/spf13/cobra"
	cfsslapi "gitlab.com/andrewheberle/cfssl-api"
)

// infoCmd represents the info command
var (
	// Used for flags.
	infoProfile string
	infoLabel   string

	infoCmd = &cobra.Command{
		Use:   "info",
		Short: "A brief description of your command",
		Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
		Run: func(cmd *cobra.Command, args []string) {
			api := cfsslapi.NewApiClient(remote, nil)
			resp, err := api.InfoProfile(infoLabel, infoProfile)
			if err != nil {
				fmt.Printf("error: %s\n", err)
				return
			}

			fmt.Printf("Certificate: %s; Expiry: %s; Usages: %s\n", resp.Certificate, resp.Expiry, strings.Join(resp.Usages, ", "))
		},
	}
)

func init() {
	rootCmd.AddCommand(infoCmd)

	infoCmd.Flags().StringVarP(&infoLabel, "label", "l", "primary", "CA label")
	infoCmd.Flags().StringVarP(&infoProfile, "profile", "p", "default", "CA profile")
}
