package cmd

import (
	"fmt"
	"os"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// rootCmd represents the base command when called without any subcommands
var (
	// Used for flags.
	cfgFile string
	remote  string

	rootCmd = &cobra.Command{
		Use:   "cfssl-api",
		Short: "CFSSL API test tool",
		Long: `This CLI tool allows you to perform requests against
a CFSSL instance.`,
		// Uncomment the following line if your bare application
		// has an action associated with it:
		//	Run: func(cmd *cobra.Command, args []string) { },
	}
)

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.cfssl-api.yaml)")
	rootCmd.PersistentFlags().StringVar(&remote, "remote", "localhost:8000", "remote CFSSL instance")
	viper.BindPFlag("useViper", rootCmd.PersistentFlags().Lookup("viper"))
}

func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			fmt.Printf("Could not find gome directory: %s\n", err)
			os.Exit(1)
		}

		// Search config in home directory with name ".cfssl-api" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigName(".cfssl-api")
	}

	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}
