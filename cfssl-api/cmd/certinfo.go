/*
Copyright © 2019 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"strings"

	"github.com/spf13/cobra"
	cfsslapi "gitlab.com/andrewheberle/cfssl-api"
)

var (
	// Used for flags.
	certInfoDomain string

	// certinfoCmd represents the certinfo command
	certinfoCmd = &cobra.Command{
		Use:   "certinfo",
		Short: "A brief description of your command",
		Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
		Run: func(cmd *cobra.Command, args []string) {
			api := cfsslapi.NewApiClient(remote, nil)
			resp, err := api.CertInfoDomain(certInfoDomain)
			if err != nil {
				fmt.Printf("error: %s\n", err)
				return
			}

			fmt.Printf("Subject.CommonName: %s\nSans = %s\nNotBefore = %s\nNotAfter = %s\nSignatureAlgorithm = %s\n", resp.Subject.CommonName, strings.Join(resp.Sans, ", "), resp.NotBefore, resp.NotAfter, resp.SignatureAlgorithm)
		},
	}
)

func init() {
	rootCmd.AddCommand(certinfoCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// certinfoCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// certinfoCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	certinfoCmd.Flags().StringVarP(&certInfoDomain, "domain", "d", "", "Remote host to retrieve a certificate for")
}
