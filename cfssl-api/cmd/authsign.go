package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	cfsslapi "gitlab.com/andrewheberle/cfssl-api"
)

// signCmd represents the sign command
var (
	// Used for flags.
	authSignCsr     string
	authSignToken   string
	authSignLabel   string
	authSignProfile string

	authSignCmd = &cobra.Command{
		Use:   "authsign",
		Short: "A brief description of your command",
		Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
		Run: func(cmd *cobra.Command, args []string) {
			api := cfsslapi.NewApiClient(remote, nil)
			resp, err := api.AuthSign(authSignToken, authSignCsr, cfsslapi.SignOptions{Label: authSignLabel, Profile: authSignProfile})
			if err != nil {
				fmt.Printf("error: %s\n", err)
				return
			}

			fmt.Printf("Certificate: %s;\n", resp.Certificate)
		},
	}
)

func init() {
	rootCmd.AddCommand(authSignCmd)

	authSignCmd.Flags().StringVarP(&authSignCsr, "request", "r", "", "Certificate Signing Request")
	authSignCmd.Flags().StringVarP(&authSignLabel, "label", "l", "primary", "CA label")
	authSignCmd.Flags().StringVarP(&authSignProfile, "profile", "p", "default", "CA profile")
	authSignCmd.Flags().StringVarP(&authSignToken, "token", "t", "", "Authentication token")
}
