package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	cfsslapi "gitlab.com/andrewheberle/cfssl-api"
)

// signCmd represents the sign command
var (
	// Used for flags.
	signCsr     string
	signLabel   string
	signProfile string

	signCmd = &cobra.Command{
		Use:   "sign",
		Short: "A brief description of your command",
		Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
		Run: func(cmd *cobra.Command, args []string) {
			api := cfsslapi.NewApiClient(remote, nil)
			resp, err := api.Sign(signCsr, cfsslapi.SignOptions{Label: signLabel, Profile: signProfile})
			if err != nil {
				fmt.Printf("error: %s\n", err)
				return
			}

			fmt.Printf("Certificate: %s;\n", resp.Certificate)
		},
	}
)

func init() {
	rootCmd.AddCommand(signCmd)

	signCmd.Flags().StringVarP(&signCsr, "request", "r", "", "Certificate Signing Request")
	signCmd.Flags().StringVarP(&signLabel, "label", "l", "primary", "CA label")
	signCmd.Flags().StringVarP(&signProfile, "profile", "p", "default", "CA profile")
}
