package main

import "gitlab.com/andrewheberle/cfssl-api/cfssl-api/cmd"

func main() {
	cmd.Execute()
}
