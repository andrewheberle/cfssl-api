package cfsslapi

import (
	"fmt"

	errors "golang.org/x/xerrors"
)

/*
THE REVOKE ENDPOINT

Endpoint: /api/v1/cfssl/revoke
Method:   POST

Required parameters:

    * serial: a string specifying the serial number of a certificate
    * authority_key_id: a string specifying the authority key identifier
      of the certificate to be revoked; this is used to distinguish
      which private key was used to sign the certificate.
    * reason: a string identifying why the certificate was revoked; see,
      for example, ReasonStringToCode in the ocsp package or section
      4.2.1.13 of RFC 5280. The "reasons" used here are the ReasonFlag
      names in said RFC.

Result:

    The returned result is an empty JSON object
*/

// RevocationReason constants from RFC 5280
const (
	RevokeReasonKeyCompromise      = "keyCompromise"
	RevokeReasonCACompromise       = "cACompromise"
	RevokeReasonAffiliationChanged = "affiliationChanged"
	RevokeReasonSuperceded         = "superseded"
	RevokeReasonCeaseOperation     = "cessationOfOperation"
	RevokeReasonCertificateHold    = "certificateHold"
	RevokeReasonPrivilegeWithdrawn = "privilegeWithdrawn"
	RevokeReasonAACompromise       = "aACompromise"
)

// revokeRequest represents a revocation request
// All parameters are required to revoke a certificate
type revokeRequest struct {
	Serial       string `json:"serial"`
	AuthorityKey string `json:"authority_key_id"`
	Reason       string `json:"reason"`
}

// Revoke performs the revocation of a certificate
func (api *ApiClient) Revoke(serial, authoritykey, reason string) (err error) {
	var endpoint = "revoke"

	if serial == "" {
		return fmt.Errorf("missing serial")
	}

	if authoritykey == "" {
		return fmt.Errorf("missing authority_key_id")
	}

	if reason == "" {
		return fmt.Errorf("missing reason")
	}

	_, err = api.doRequest(revokeRequest{
		Serial:       serial,
		AuthorityKey: authoritykey,
		Reason:       reason,
	}, endpoint)
	if err != nil {
		return err
	}

	_, err = api.doRequest(revokeRequest{
		Serial:       serial,
		AuthorityKey: authoritykey,
		Reason:       reason,
	}, endpoint)
	if err != nil {
		return errors.Errorf("%s: %w", endpoint, err)
	}

	return nil
}
