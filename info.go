package cfsslapi

/*
THE INFO ENDPOINT

Endpoint: /api/v1/cfssl/info
Method:   POST

Required parameters:

    * label: a string specifying the signer

Optional parameters:

    * profile: a string specifying the signing profile for the signer.
    Signing profile specifies what key usages should be used and
    how long the expiry should be set

Result:

    The returned result is a JSON object with three keys:

    * certificate: a PEM-encoded certificate of the signer
    * usage: a string array of key usages from the signing profile
    * expiry: the expiry string from the signing profile
*/

import (
	"encoding/json"

	errors "golang.org/x/xerrors"
)

type infoRequest struct {
	Label   string `json:"label"`
	Profile string `json:"profile,omitempty"`
}

type InfoResponse struct {
	Certificate string   `json:"certificate"`
	Expiry      string   `json:"expiry"`
	Usages      []string `json:"usages"`
}

func (api *ApiClient) info(req infoRequest) (resp InfoResponse, err error) {
	var endpoint = "info"

	r, err := api.doRequest(req, endpoint)
	if err != nil {
		return resp, errors.Errorf("%s: %w", endpoint, err)
	}

	err = json.Unmarshal(r.Result, &resp)
	if err != nil {
		return resp, errors.Errorf("%s: %w", endpoint, err)
	}

	return resp, nil
}

func (api *ApiClient) Info(label string) (resp InfoResponse, err error) {
	return api.info(infoRequest{
		Label: label,
	})
}

func (api *ApiClient) InfoProfile(label, profile string) (resp InfoResponse, err error) {
	return api.info(infoRequest{
		Label:   label,
		Profile: profile,
	})
}
