package cfsslapi

/*
THE SIGNING ENDPOINT

Endpoint: /api/v1/cfssl/sign
Method:   POST

Required parameters:

    * certificate_request: the CSR bytes to be signed in PEM

Optional parameters:

    * hosts: an array of SAN (subject alternative names)
    which overrides the ones in the CSR
    * subject: the certificate subject which overrides
    the ones in the CSR
    * serial_sequence: a string specify the prefix which the generated
    certificate serial should have
    * label: a string specifying which signer to be appointed to sign
    the CSR, useful when interacting with a remote multi-root CA signer
    * profile: a string specifying the signing profile for the signer,
    useful when interacting with a remote multi-root CA signer
    * bundle: a boolean specifying whether to include an "optimal"
    certificate bundle along with the certificate

Result:

    The returned result is a JSON object with a single key:

    * certificate: a PEM-encoded certificate that has been signed
    by the server.
    * bundle: See the result of endpoint_bundle.txt (only included if the bundle parameter was set)
*/

import (
	"encoding/json"

	errors "golang.org/x/xerrors"
)

type signRequest struct {
	CertificateRequest string   `json:"certificate_request"`
	Hosts              []string `json:"hosts,omitempty"`
	Subject            string   `json:"subject,omitempty"`
	SerialSequence     string   `json:"serial_sequence,omitempty"`
	Label              string   `json:"label,omitempty"`
	Profile            string   `json:"profile,omitempty"`
	Bundle             bool     `json:"bundle,omitempty"`
}

type SignOptions struct {
	Label   string
	Profile string
}

type SignResponse struct {
	Certificate string `json:"certificate"`
	Bundle      string `json:"bundle"`
}

func newSignRequest(csr string, options interface{}) (req signRequest) {
	req.CertificateRequest = csr

	if opts, ok := options.(SignOptions); ok {
		if opts.Label != "" {
			req.Label = opts.Label
		}

		if opts.Profile != "" {
			req.Profile = opts.Profile
		}
	}

	return
}

func (api *ApiClient) sign(req signRequest) (resp SignResponse, err error) {
	var endpoint = "sign"

	r, err := api.doRequest(req, endpoint)
	if err != nil {
		return resp, errors.Errorf("%s: %w", endpoint, err)
	}

	err = json.Unmarshal(r.Result, &resp)
	if err != nil {
		return resp, errors.Errorf("%s: %w", endpoint, err)
	}

	return resp, nil
}

// Sign peforms
func (api *ApiClient) Sign(csr string, options interface{}) (resp SignResponse, err error) {
	return api.sign(newSignRequest(csr, options))
}
